package jdbc;

public class User extends Role {
    private Long id;
    private String login;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDay;

    @Override
    public final Long getId() {
        return id;
    }

    @Override
    public final void setId(final Long id) {
        this.id = id;
    }

    public final String getLogin() {
        return login;
    }

    public final void setLogin(final String login) {
        this.login = login;
    }

    public final String getPassword() {
        return password;
    }

    public final void setPassword(final String password) {
        this.password = password;
    }

    public final String getEmail() {
        return email;
    }

    public final void setEmail(final String emailfirstName) {
        this.email = emailfirstName;
    }

    public final String getFirstName() {
        return firstName;
    }

    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public final String getLastName() {
        return lastName;
    }

    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public final String getBirthDay() {
        return birthDay;
    }

    public final void setBirthDay(final String birthDay) {
        this.birthDay = birthDay;
    }
}
