package jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Properties;


public class JdbcRoleDao extends AbstractjdbcDao implements RoleDao {
    private Properties prop;

    public JdbcRoleDao() {
        prop = new Properties();
        try {
            prop.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    final Connection createConnection() {
        return getConnection(prop.getProperty("db.driver"),
                prop.getProperty("db.url"),
                prop.getProperty("db.username"),
                prop.getProperty("db.password"));
    }

    static Connection getConnection(final String property,
                                    final String property2,
                                    final String property3,
                                    final String property4) {
        try {
            Class.forName(property);
            return DriverManager.getConnection(property2,
                    property3,
                    property4);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public final void create(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("INSERT INTO ROLE VALUES(" + role.getId() + " , '"
                    + role.getName() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void update(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("UPDATE ROLE SET NAME='" + role.getName()
                    + "' WHERE ID=" + role.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void remove(final Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM ROLE" + " WHERE ID=" + role.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final Role findByName(final String name) {
        Role role = new Role();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM "
                     + "ROLE WHERE NAME='"
                     + name + "';")) {
            resultSet.next();
            role.setId(resultSet.getLong("ID"));
            role.setName(resultSet.getString("NAME"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return role;
    }
}
