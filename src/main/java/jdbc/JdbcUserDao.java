package jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static jdbc.JdbcRoleDao.getConnection;


public class JdbcUserDao extends AbstractjdbcDao implements UserDao {
    private Properties prop;

    public JdbcUserDao() {
        prop = new Properties();
        try {
            prop.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final Connection createConnection() {
        return getConnection(prop.getProperty("db.driver"),
                prop.getProperty("db.url"),
                prop.getProperty("db.username"),
                prop.getProperty("db.password"));
    }

    @Override
    public final void create(final User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("INSERT INTO USER VALUES(" + user.getId() + " , '"
                    + user.getLogin() + "', '"
                    + user.getPassword() + "', '"
                    + user.getEmail() + "', '"
                    + user.getFirstName() + "', '"
                    + user.getLastName() + "', '"
                    + user.getBirthDay() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void update(final User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("UPDATE USER SET LOGIN='" + user.getLogin()
                    + "', PASSWORD='"
                    + user.getPassword()
                    + "', EMAIL='" + user.getEmail()
                    + "', FIRSTNAME='" + user.getFirstName()
                    + "', LASTNAME='" + user.getLastName()
                    + "', BIRTHDAY='" + user.getBirthDay()
                    + "' WHERE ID=" + user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void remove(final User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM USER" + " WHERE ID=" + user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        User user = null;
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement
                     .executeQuery("SELECT * FROM USER;")) {
            while (resultSet.next()) {
                user = new User();
                initItems(user, resultSet);
                allUsers.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return allUsers;
    }

    @Override
    public final User findByLogin(final String login) {
        User user = new User();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement
                     .executeQuery("SELECT * FROM USER "
                             + "WHERE LOGIN='" + login + "';")) {
            resultSet.next();
            initItems(user, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return user;
    }

    @Override
    public final User findByEmail(final String email) {
        User user = new User();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement
                     .executeQuery("SELECT * FROM USER "
                             + "WHERE EMAIL='" + email + "';")) {
            resultSet.next();
            initItems(user, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return user;
    }

    final void initItems(final User user,
                         final ResultSet resultSet) throws SQLException {
        user.setId(resultSet.getLong("ID"));
        user.setLogin(resultSet.getString("LOGIN"));
        user.setPassword(resultSet.getString("PASSWORD"));
        user.setEmail(resultSet.getString("EMAIL"));
        user.setFirstName(resultSet.getString("FIRSTNAME"));
        user.setLastName(resultSet.getString("LASTNAME"));
        user.setBirthDay(resultSet.getString("BIRTHDAY"));
    }

//    public void createTable() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("CREATE TABLE USER\n" +
//                    "(\n" +
//                    "    ID BIGINT PRIMARY KEY,\n" +
//                    "    LOGIN VARCHAR(255),\n" +
//                    "    PASSWORD VARCHAR(255),\n" +
//                    "    EMAIL VARCHAR(255),\n" +
//                    "    FIRSTNAME VARCHAR(255),\n" +
//                    "    LASTNAME VARCHAR(255),\n" +
//                    "    BIRTHDAY VARCHAR(255)\n" +
//                    ");");
//            statement.execute("    CREATE TABLE ROLE\n" +
//                    "            (\n" +
//                    "                    ID BIGINT PRIMARY KEY,\n" +
//                    "                    NAME VARCHAR(255),\n" +
//                    ");");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

}
